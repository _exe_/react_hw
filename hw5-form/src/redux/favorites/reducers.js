import types from "./types";

const initialState = [];

const favoritesReducer = (currentState = initialState, action) => {
    switch (action.type) {
        case types.SAVE_FAV_ITEM:
            return [...currentState, action.payload]
        case types.REMOVE_FAV_ITEM:
            return currentState.filter(item => item !== action.payload);
        default:
            return currentState
    }
};

export default favoritesReducer;