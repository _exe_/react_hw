import types from "./types";



const saveFavItem = (fav) => ({
    type: types.SAVE_FAV_ITEM,
    payload: fav
})
const removeFavItem = (fav)=> ({
    type: types.REMOVE_FAV_ITEM,
    payload: fav
})

const saveFavItemAction = (favItem) => (dispatch, getState) => {
    dispatch(saveFavItem(favItem));
}

const removeFavItemAction = (favItem) => dispatch => {
    dispatch(removeFavItem(favItem))
}


export  default ({
    saveFavItem: saveFavItemAction,
    removeFavItem: removeFavItemAction
});


