import types from "./types";

//action creator
const toggleModalOpen = (bol) => ({
    type: types.TOGGLE_MODAL,
    payload: bol
});


//action
const toggleModalOpenAction = () => (dispatch, getState)=> {
        dispatch(toggleModalOpen(!getState().modal))
}


export default ({
    toogleModal: toggleModalOpenAction
});