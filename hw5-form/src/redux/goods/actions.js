import types from "./types";




//ACTION CREATOR
const toggleModalAction = isModalOpen => ({
    type: types.TOGGLE_MODAL,
    payload: isModalOpen
})


const saveAllGoods = goods => ({
    type: types.SAVE_ALL_GOODS,
    payload: goods
})


const saveAllGoodsAction = (goods) => (dispatch) =>  {
        fetch('/goods.json')
            .then(r=> r.json())
            .then(data=> {
                dispatch(saveAllGoods(data.result))
            })
};



export  default ({
    saveAllGoods: saveAllGoodsAction,
    toggleModal: toggleModalAction
});


