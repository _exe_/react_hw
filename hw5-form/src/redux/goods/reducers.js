import types from "./types";

const initialState = []

const goodsReducer = (currentState = initialState, action) => {
    switch (action.type) {
        case types.SAVE_ALL_GOODS:
            return  action.payload

        default:
            return currentState
    }
};



export default goodsReducer