import types from "./types";

const saveCartItemAction = (cartItem) => ({
    type: types.SAVE_CART_ITEM,
    payload: cartItem
})

const removeCartItemAction = (cartItem)=> ({
    type: types.REMOVE_CART_ITEM,
    payload: cartItem
});
const removeAllCartItemsAction = () => ({
    type: types.REMOVE_ALL_CART_ITEMS
})

const saveCartItem = (cartItem) => dispatch =>  {
    dispatch(saveCartItemAction(cartItem));
}

const removeCartItem = (cartItem) => dispatch => {
    dispatch(removeCartItemAction(cartItem))
}

const removeAllCartItems = () => dispatch => {
    dispatch(removeAllCartItemsAction());
}

export  default ({
    saveCartItemAction,
    removeCartItemAction,
    saveCartItem,
    removeCartItem,
    removeAllCartItems
});


