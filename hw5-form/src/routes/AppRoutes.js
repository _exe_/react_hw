import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Cart from '../components/Cart/Cart'
import Favorites from '../components/Favorites/Favorites'
import GoodsList from '../components/Goods/GoodsList'
import Page404 from '../components/pages/Page404'

const AppRoutes = () => {
      return (
        <>
          <Switch>
            <Route exact path="/" component={GoodsList}/>
            <Route exact path="/cart" component={Cart} />
            <Route exact path="/favorites" component={Favorites} />
            <Route path="*" component={Page404} />
          </Switch>
        </>
      )
}


export default AppRoutes;
