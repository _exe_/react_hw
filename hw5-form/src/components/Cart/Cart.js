import React, {useEffect, useState} from 'react'
import Modal from '../Modal/Modal';
import CartItem from './CartItem';
import {useDispatch, useSelector} from "react-redux";
import {cartActions, cartSelectors} from "../../redux/cart";
import {goodsActions, goodsSelectors} from "../../redux/goods";
import {modalActions, modalSelectors} from "../../redux/modal";
import CheckoutForm from "../Form/CheckoutForm";

const Cart = () => {
  const dispatch = useDispatch();
  const isModalOpenSelector = useSelector(modalSelectors.selectModal)
  const allItems = useSelector(goodsSelectors.allGoods);
  const allCartItemsSelector = useSelector(cartSelectors.allCart);

  const [cartItems, setCartItems] = useState([]);
  const [choosenGood, setChoosenGood] = useState(null);

  useEffect( ()=> {
    dispatch(goodsActions.saveAllGoods())
  }, [])

  useEffect(()=> {
    const cartItemsObj = allItems.filter((item) => allCartItemsSelector.includes(item.id));
    setCartItems(cartItemsObj)
  }, [allItems, choosenGood, allCartItemsSelector])

  const  handleModal = () => {
    dispatch(modalActions.toogleModal)
  }

  const getChoosenGood = (itemId) => {
    setChoosenGood(itemId)
  }

  const removeFromCart = () => {
    dispatch(cartActions.removeCartItem(choosenGood))
    setChoosenGood(null)
  }

  const cartElements = cartItems.map(item=> {
        return (<CartItem
            key={item.id}
            id={item.id}
            name={item.name}
            image={item.image}
            price={item.price}
            setNumber={item.setNumber}
            color={item.color}
            removeFromCart={(goodItemId)=> getChoosenGood(goodItemId)}
            openModal={handleModal}
        />)
      }
  );

  return (
    <div>
      { cartElements.length !== 0 ?
          <>
            {cartElements}
              <CheckoutForm/>
          </>
          :
          <h1>No items in  cart</h1>}
      {isModalOpenSelector &&
        <Modal
            handleConfirmAction={()=> removeFromCart()}
            header={"Remove from cart"}
            closeButton={true}
            text={"Are u shure?"}
        />
      }
    </div>
  )
}

export default Cart
