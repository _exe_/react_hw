import React, { useEffect, useState } from 'react';
import "./Goods.scss";
import GoodsItem from './GoodsItem';
import Modal from '../Modal/Modal';
import {connect, useDispatch, useSelector} from "react-redux";
import {goodsActions, goodsSelectors} from "../../redux/goods";
import {favActions, favSelectors} from "../../redux/favorites";
import {modalActions, modalSelectors} from "../../redux/modal";
import {cartActions} from "../../redux/cart";



const GoodsList = () => {
  const dispatch = useDispatch();
  const allGoodsSelector = useSelector(goodsSelectors.allGoods);
  const allFavoriteItems = useSelector(favSelectors.allFavorites);
  const isModalOpenSelector = useSelector(modalSelectors.selectModal);
  const [choosenGood, setChoosenGood] = useState([]);

  useEffect(()=> {
    dispatch(goodsActions.saveAllGoods())
  }, [])


  const getFavorites = (goodsItem) => {
    const result = allFavoriteItems.find(fav => fav === goodsItem);
    return result ? true : false;
  }

  const getChoosenGood = (itemId) => {
    setChoosenGood(itemId)
  }

  const addToCart = () => {
    dispatch(cartActions.saveCartItem(choosenGood))
  }

  const handleModal = () => {
    dispatch(modalActions.toogleModal())
  }

  const handleFavItem = (goodItemId) =>  {
    const isIDinFavorites = allFavoriteItems.some(item => item === goodItemId);
    if (isIDinFavorites){
      dispatch(favActions.removeFavItem(goodItemId))
    }else {
      dispatch(favActions.saveFavItem(goodItemId));
    }
  }


  const goodsListArr = allGoodsSelector.map((good) => {
    return(
    <GoodsItem 
          key={good.id} 
          isWished={getFavorites(good.id)}
          id={good.id} 
          name={good.name} 
          image={good.image} 
          price={good.price} 
          setNumber={good.setNumber} 
          color={good.color}
          addToCartProp={(goodItemId)=> getChoosenGood(goodItemId)}
          addToFavProp = {(goodItemId)=> handleFavItem(goodItemId)}
          openModal={handleModal}
        />  
    )
   })


  return (
    <>
      <div className="goods">
        {goodsListArr}
      </div>
      {isModalOpenSelector &&
        <Modal
            handleConfirmAction={()=> addToCart()}
            header={"Add to cart"}
            closeButton={true}
            text={"Are u  shure?"}
        />
      }
    </>
  );
}

export default connect()(GoodsList)
