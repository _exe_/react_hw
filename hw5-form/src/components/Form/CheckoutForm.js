import React from 'react';
import {Formik,  Form, Field} from "formik";
import schemas from "./validationSchema";
import "./CheckoutForm.scss";
import CheckoutInput from "./CheckoutInput";
import {useDispatch, useSelector} from "react-redux";
import {cartActions, cartSelectors} from "../../redux/cart";

const CheckoutForm = () => {
    const dispatch = useDispatch();
    const allCartItemsSelector = useSelector(cartSelectors.allCart);
    const handleSubmit = (values, {setSubmitting, resetForm}) => {
        const checkoutObj = {values, selectedGoods: allCartItemsSelector};
        console.log(checkoutObj);
        dispatch(cartActions.removeAllCartItems())
        setSubmitting(false);
        resetForm();
    }
    return (
        <>
            <Formik
                initialValues={{ username: '', userSurname: '', userAge: '', deliveryAddress: '', phoneNumber: '' }}
                validationSchema={schemas}
                onSubmit={handleSubmit}
            >
                {({handleChange, onSubmit, isValid,dirty,  isSubmitting}) => (
                    <Form className="form">
                        <label className="form__label" htmlFor="username">Your name</label>
                        <Field
                            component={CheckoutInput}
                            type="text"
                            name="username"
                            onChange={handleChange}
                        />
                        <label htmlFor="userSurname">Your surname</label>
                        <Field
                            component={CheckoutInput}
                            type="text"
                            name="userSurname"
                            onChange={handleChange}
                        />

                        <label htmlFor="userAge">Your age</label>
                        <Field
                            component={CheckoutInput}
                            type="text"
                            name="userAge"
                            onChange={handleChange}
                        />
                        <label htmlFor="deliveryAddress">Delivery Address</label>
                        <Field
                            component={CheckoutInput}
                            type="text"
                            name="deliveryAddress"
                            onChange={handleChange}
                        />
                        <label htmlFor="phoneNumber"  >Phone number</label>
                        <Field
                            component={CheckoutInput}
                            type="text"
                            name="phoneNumber"
                            onChange={handleChange}
                            maxLength={13}
                        />
                        <button type="submit" disabled={!isValid || !dirty || isSubmitting}>
                            Checkout
                        </button>
                    </Form>
                )}
            </Formik>
        </>
    );
};


export default CheckoutForm;