import React from 'react';
import "./CheckoutForm.scss"

const CheckoutInput = ({form, field, ...rest}) => {
    const {name} = field;
    return (
        <>
            {
                form.touched[name]
                && form.errors[name]
                && <span className="form__error">{form.errors[name]}</span>
            }
            <input className="form__input" {...field} {...rest} />
        </>
    );
};

export default CheckoutInput;