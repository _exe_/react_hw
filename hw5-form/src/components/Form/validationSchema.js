import * as yup from "yup";

const schema = yup.object().shape({
    username: yup.string().required("this fill is required").min(2, "Min length is 2"),
    userSurname: yup.string().required("this fill is required").min(3, "Min length is 3"),
    userAge: yup.number().typeError('age must be a number').positive().integer("Must be an integer").min(18, "Users age can't be less then 18"),
    deliveryAddress: yup.string()
        .trim()
        .matches(/[abcdefghijklmnopqrstuvwxyz]+/ , 'Is not in correct format')
        .required(),
    phoneNumber: yup.string().min(13, "Must be 13 characters").max(13, "Invalid phone number, length is too long").required()
});

export  default schema;