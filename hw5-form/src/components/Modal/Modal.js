import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import {connect, useDispatch} from "react-redux";
import {modalActions} from "../../redux/modal";



const Modal = ({header, handleConfirmAction,  closeButton, text }) => {
    const dispatch = useDispatch();
    const handleClose = (e)=> {
        e.stopPropagation()
        dispatch(modalActions.toogleModal())
    }

    const handleConfirmed = () => {
        handleConfirmAction();
    }

    return (
        <div onClick={handleClose} className="modal__wrapper">
            <div className="modal">
                <div className="modal__header">
                    <h3>{header}</h3>
                    {closeButton && <Button styleSelectors={"btn-cancel btn"} text={"CLOSE"}  onClick={handleClose}  />}
                </div>
                <p className="modal__content">{text}</p>
                <div className="modal__actions">
                    <Button styleSelectors={"btn-confirm btn"}  onClick={handleConfirmed} backgroundColor={'green'} text={"OK"}/>
                    <Button  styleSelectors={"btn-cancel btn"} backgroundColor={"red"} text={"CANCEL"} onClick={handleClose} />
                </div>
            </div>
        </div>
    );
};

Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string.isRequired,

}

export default connect()(Modal);