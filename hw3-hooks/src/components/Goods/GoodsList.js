import React, { useEffect, useState } from 'react';
import "./Goods.scss";
import GoodsItem from './GoodsItem';
import Modal from '../Modal/Modal';



const GoodsList = () => {
  const [choosenToCart, setChoosenToCart] = useState(null);
  const [choosenToFav, setChoosenToFav] = useState(null);
  const [goods, setGoods] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);

  useEffect(() => {
    fetch("/goods.json")
    .then(res=> res.json())
    .then(data=> { 
        if (!localStorage["items"]) {
          localStorage.setItem("items", JSON.stringify(data.result))
          setGoods(data.result)
        } else{
          setGoods(data.result)
        }}
      )
    .catch(err=> console.log(err));
  }, []);


  useEffect(()=> {
    if (choosenToFav !== null) {
      handleFavorites(choosenToFav)
    }
  }, [choosenToFav]);

  useEffect(()=> {
    if (choosenToCart !== null) {
      setChoosenToCart(choosenToCart)
    }
  }, [choosenToCart]);

  const handleClick = () => {
    setIsModalOpen(!isModalOpen);
  };

  const shadowClick = () => {
    if (isModalOpen) {
      setIsModalOpen(!isModalOpen);
    }
  };

  const handleModal = () => {
    setIsModalOpen(!isModalOpen);
  };


  const addToCart = (item) =>{
    handleCart(item);
  };


  const handleCart = (item)=> {
    if (localStorage) {
      let cart;
      if (!localStorage["cart"]) {
        cart = [];
      } else {
        cart = JSON.parse(localStorage['cart']);
      }            
      if (!(cart instanceof Array)) {cart = [];}
      cart.push(item);
      localStorage.setItem('cart', JSON.stringify(cart));
  } 
};

const handleFavorites = (favId)=> {
  let newList = [];
  if (localStorage) {
    const favList = JSON.parse(localStorage.getItem("wishes")) || [];
    const result = favList.some(item => item === favId);
    if (result) {
      newList = favList.filter(i=> i !== favId);
      localStorage.setItem("wishes", JSON.stringify(newList));          
    } else{
      newList = [...favList, favId];
      localStorage.setItem("wishes", JSON.stringify(newList));
    }
  }
  setChoosenToFav(null);
};


  const getFavorites = (goodsItem) => {
    const favoritesArr = JSON.parse(localStorage.getItem("wishes")) || [];
    const result = favoritesArr.find(fav => fav === goodsItem);
    return result ? true : false;    
  } 

  const goodsListArr = goods.map((good) => {
    return(
    <GoodsItem 
          key={good.id} 
          isWished={getFavorites(good.id)} 
          id={good.id} 
          name={good.name} 
          image={good.image} 
          price={good.price} 
          setNumber={good.setNumber} 
          color={good.color} 
          setChoosenToCart={(goodItem)=> setChoosenToCart(goodItem)} 
          setChoosenToFav={(goodItem)=> setChoosenToFav(goodItem)} 
          openModal={()=> handleModal()} 
        />  
    )
   })

  return (
    <>
      <div className="goods">
        {goodsListArr}
      </div>
      {isModalOpen && 
        <Modal 
        choosenGood={choosenToCart} 
        shadowClick={(e) => shadowClick(e)} 
        header={"Add to cart"} 
        handleClose={() => handleClick()} 
        closeButton={true} 
        text={"Are u  shure?"} 
        addToCart={(e) => addToCart(e)} 
        />
      }
    </>
  );
}

export default GoodsList
