import React from 'react';
import PropTypes from 'prop-types';
import "./Goods.scss";
import Button from '../Button/Button';


const GoodsItem = ({id, name, image, price, setNumber, color, openModal,   isWished, setChoosenToCart, setChoosenToFav})=> {
  const getItem = () =>{
    setChoosenToCart(id);
  };

  const getWishesItem = ()=> {
    setChoosenToFav(id);
  };

  return (
    <div className="goods__item">
      <img src={image} alt={image} className="goods__picture" />
      <p className="goods__group">price: {price}</p>
      <p className="goods__group">Name: {name}</p>
      <p className="goods__group">Set number: {setNumber}</p>
      <p className="goods__group">Color: {color}</p>
      <div className="goods__actions">
        <Button 
          styleSelectors={isWished ? "star__active btn" : "star btn"}   
          onClick={()=> {getWishesItem();}} 
        />
        <Button 
          styleSelectors={"btn"} 
          background={"#2C523E"} text={"Add to cart"} 
          onClick={()=> {getItem(); openModal();}} 
        />
      </div>
    </div>
  );
}

GoodsItem.defaultProps = {
  color: 'blue'
};

GoodsItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  setNumber: PropTypes.string.isRequired,
  color: PropTypes.string,
  isWished: PropTypes.bool.isRequired,
  setChoosenToCart: PropTypes.func.isRequired,
  setChoosenToFav: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired

};

export default GoodsItem;