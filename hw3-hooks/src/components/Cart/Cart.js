import React, {useEffect, useState} from 'react'
import checkGoodsFromLS from '../../utils/utils';
import Modal from '../Modal/Modal';
import CartItem from './CartItem';

const Cart = () => {
  const [cart, setCart] = useState([]);
  const [choosenGood, setChoosenGood] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);


  
  useEffect(() => {
    const cart = JSON.parse(localStorage.getItem("cart")) || [];
    const allItems = JSON.parse(localStorage.getItem("items")) || [];
    let cartItems = [];
    if (allItems.length !== 0) {
      cartItems = allItems.filter(item=> checkGoodsFromLS(item.id, cart) && item);
    }
    setCart(cartItems);
  }, [choosenGood]);

  const handleClick = () => {
    setIsModalOpen(!isModalOpen);
  };

  const shadowClick = () => {
    if (isModalOpen) {
      setIsModalOpen(!isModalOpen);
    }
  };

  const handleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  const choosenGoodF = (goodItem)=>{
    setChoosenGood(goodItem);
  };


  const removeCartFromLS = (item)=> {
    if (localStorage) {
      const cartList = JSON.parse(localStorage.getItem("cart"));
      const newList = cartList.filter(i=> i !== item);
      localStorage.setItem('cart', JSON.stringify(newList));
      setChoosenGood(null)
    }

  };

    const cartItemsEl = cart.map(item=> (
      <CartItem
        key={item.id}
        id={item.id}
        name={item.name}
        image={item.image}
        price={item.price}
        setNumber={item.setNumber}
        setChoosenGood={(goodItem)=> choosenGoodF(goodItem)} 
        color={item.color} 
        openModal={()=> handleModal()}
        />
    )
    );


  return (
    <div>
      {cartItemsEl.length !== 0 ?  cartItemsEl : <h1>Sorry, no items in cart!</h1> }
      {isModalOpen && 
        <Modal 
        choosenGood={choosenGood} 
        shadowClick={(e) => shadowClick(e)} 
        header={"Remove from cart"} 
        handleClose={() => handleClick()} 
        closeButton={true} 
        text={"Are u  shure?"} 
        addToCart={(e) => removeCartFromLS(e)} 
        />
      }
    </div>
  )
}

export default Cart
