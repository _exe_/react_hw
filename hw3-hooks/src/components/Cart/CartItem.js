import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import "./CartItem.scss";


const CartItem = ({id, name, image, price, setNumber, color, setChoosenGood,  openModal}) => {
  const getItem = () =>{
    setChoosenGood(id);
  };
  
  return (
    <div className="cart__item" >
        <img src={image} alt={image} className="cart__picture"/>
        <div className="cart__details">
          <p className="cart__details">Price {price}</p>
          <p className="cart__details">Set number {setNumber}</p>
          <p className="cart__details">Name {name}</p>
          <p className="cart__details">Color {color}</p>
        </div> 
        <div className="cart__action">
        <Button 
          styleSelectors={"btn btn-remove"} 
          text={"Remove"} 
          onClick={()=> {getItem(); openModal();}} 
        />
        </div>
    </div>
  )
}

CartItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  setNumber: PropTypes.string.isRequired,
  color: PropTypes.string,
  setChoosenGood: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired
}

export default CartItem;

