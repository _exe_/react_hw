import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';

class Modal extends Component {
 
  render() {
    const {header, handleClose, closeButton, text} = this.props;
    return (
      <div onClick={(e) => this.shadowHandleClick(e)} className="modal__wrapper">
        <div className="modal">
            <div className="modal__header">
              <h3>{header}</h3>
              {closeButton && <Button styleSelectors={"btn-cancel btn"} text={"CLOSE"}  onClick={()=> handleClose()}  />}
            </div>
          
            <p className="modal__content">{text}</p>
            <div className="modal__actions">

            <Button styleSelectors={"btn-confirm btn"}  onClick={()=> {this.confirmAdd(); handleClose()}} backgroundColor={'green'} text={"OK"}/>
            <Button  styleSelectors={"btn-cancel btn"} backgroundColor={"red"} text={"CANCEL"} onClick={()=> handleClose()} />
          </div>
        </div>
      </div>
    );
  }

  shadowHandleClick(event){
    if (event.target.className === "shadow") {
      this.props.shadowClick()
    }
  }
  confirmAdd(){
    const {choosenGood, addToCart} = this.props;
    addToCart(choosenGood)
  }
}

Modal.propTypes = {
  choosenGood: PropTypes.number,
  shadowClick: PropTypes.func.isRequired,
  header: PropTypes.string,
  handleClose: PropTypes.func.isRequired,
  closeButton: PropTypes.bool,
  text: PropTypes.string.isRequired,
  addToCart: PropTypes.func.isRequired

}

export default Modal;