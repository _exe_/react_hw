import React from 'react';
import Button from "../Button/Button"

const Page404 = (props) => {

  return (
    <div>
      <p>This is 404 page. Please press button</p>
      <Button text="Go back" styleSelectors="btn btn-remove" onClick={()=> props.history.goBack()}/>
    </div>
  )
}

export default Page404
