import React, { useEffect, useState} from 'react';
import checkGoodsFromLS from '../../utils/utils';
import FavItem from './FavItem';


const Favorites = () => {
  const [favorites, setFavorites] = useState([]);
  const [choosenGood, setChoosenGood] = useState(null);
  
  useEffect(()=> {
    const favorites =JSON.parse(localStorage.getItem("wishes")) || [];
    const allItems = JSON.parse(localStorage.getItem("items")) || [];
    let favItems = [];
    if (allItems.length !== 0) {
      favItems = allItems.filter((item)=> checkGoodsFromLS(item.id, favorites) && item)
    }
    setFavorites(favItems);
  }, [choosenGood]);

  useEffect(()=> {
    if (choosenGood !== null) {
      handleFavorites(choosenGood);
    } 
  }, [choosenGood]);
  

  const handleFavorites = (favId)=> {
      let newList = [] 
      if (localStorage) {
        const favList = JSON.parse(localStorage.getItem("wishes"));
        const result = favList.some(item => item === favId);
        if (result) {
          newList = favList.filter(i=> i !== favId);
          localStorage.setItem("wishes", JSON.stringify(newList));          
        } else{
          newList = [...favList, favId];
          localStorage.setItem("wishes", JSON.stringify(newList));
        }
      }
      setChoosenGood(null)

  };
  

  const favItemsEl = favorites.map(item=> (
    <FavItem
      key={item.id}
      id={item.id}
      name={item.name}
      image={item.image}
      price={item.price}
      setNumber={item.setNumber}
      color={item.color} 
      setChoosenGood={(goodItem)=> setChoosenGood(goodItem)} 
      />
  )
    );

  return (
    <div>
      {favItemsEl.length !== 0 ?  favItemsEl : <h1>No items in a favorites</h1> }
    </div>
  )
}

export default Favorites
