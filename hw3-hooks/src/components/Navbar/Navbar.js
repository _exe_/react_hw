import React from 'react'
import { NavLink } from 'react-router-dom'
import "./Navbar.scss"
function Navbar() {

  return (
    <div className="header">
      <nav className="header__nav">
      <NavLink className={"header__item"} exact to="/" activeClassName="header__item-selected">Home</NavLink>
      <NavLink className={"header__item"} exact to="/cart" activeClassName="header__item-selected">Cart</NavLink>
      <NavLink className={"header__item"} exact to="/favorites" activeClassName="header__item-selected">Favorites</NavLink>
      </nav> 
    </div>
  )
}



export default Navbar

