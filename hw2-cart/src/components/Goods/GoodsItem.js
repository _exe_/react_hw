import React, { Component } from 'react';
import PropTypes from 'prop-types';
import "./Goods.scss";
import Button from '../Button/Button';

class GoodsItem extends Component {
  render() {
    const { name, image, price, setNumber, color, openModal,  wishes, isWished } = this.props;

    return (
      <div className="goods__item">
        <img src={image} alt={image} className="goods__picture" />
        <p className="goods__group">price: {price}</p>
        <p className="goods__group">Name: {name}</p>
        <p className="goods__group">Set number: {setNumber}</p>
        <p className="goods__group">Color: {color}</p>
        <div className="goods__actions">
          <Button 
            styleSelectors={isWished ? "star__active btn" : "star btn"}   
            onClick={()=> {this.getWishesItem(); wishes();}} 
          />
          <Button 
            styleSelectors={"btn"} 
            background={"#2C523E"} text={"Add to cart"} 
            onClick={()=> {this.getItem(); openModal();}} 
          />
        </div>
      </div>
    );
  }
  getItem(){
    const { setChoosenGood, id} = this.props;
    setChoosenGood(id)
  }

  getWishesItem(){
    const { setChoosenGood, id} = this.props;
    setChoosenGood(id)
  }
}



GoodsItem.defaultProps = {
  color: 'blue'
};

GoodsItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  setNumber: PropTypes.string.isRequired,
  color: PropTypes.string,
  isWished: PropTypes.bool.isRequired,
  setChoosenGood: PropTypes.func.isRequired,
  wishes: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired

};

export default GoodsItem;