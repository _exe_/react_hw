import React, { Component } from 'react';
import PropTypes from 'prop-types';
import "./Goods.scss";
import GoodsItem from './GoodsItem';
import Modal from '../Modal/Modal';


class GoodsList extends Component {
  state = {
    choosenGood: null,
    isOpen: false
  }
  render() {
    const { isOpen, choosenGood} = this.state;

    const { goodsList} = this.props;
    const w = JSON.parse(localStorage.getItem("wishes")) || [];
    const goodsListArr = goodsList.map((good, index) => {
     let reuslt = w.find(item=> good.id === item);
      if (reuslt) {
        return (
          <GoodsItem 
            key={good.id} 
            isWished={true} 
            id={good.id} 
            name={good.name} 
            image={good.image} 
            price={good.price} 
            setNumber={good.setNumber} 
            color={good.color} 
            setChoosenGood={(goodItem)=> this.setChoosenGood(goodItem)} 
            wishes={() => this.addToWishes()} 
            openModal={()=> this.handleModal()} 
          />
        )
      } else{
       return (
         <GoodsItem 
          key={good.id} 
          isWished={false} 
          id={good.id} 
          name={good.name} 
          image={good.image} 
          price={good.price} 
          setNumber={good.setNumber} 
          color={good.color} 
          setChoosenGood={(goodItem)=> this.setChoosenGood(goodItem)} 
          wishes={() => this.addToWishes()} 
          openModal={()=> this.handleModal()} 
         />
       )
      }
     })
    return (
      <>
        <div className="goods">
          {goodsListArr}
        </div>
        {isOpen && 
          <Modal 
          choosenGood={choosenGood} 
          shadowClick={(e) => this.shadowClick(e)} 
          header={"Add to cart"} 
          handleClose={() => this.handleClick()} 
          closeButton={true} 
          text={"Are u  shure?"} 
          addToCart={(e) => this.addToCart(e)} 
          />
        }
      </>
    );
  }




  handleClick() {
    const {isOpen}= this.state;
    this.setState({isOpen: !isOpen})
  }
  
  shadowClick() {
    const { isOpen } = this.state;
    if (isOpen) {
      this.setState({ isOpen: !isOpen })
    }
  }

  handleModal(){
    const {isOpen}= this.state;
    this.setState({isOpen: !isOpen})
  }

  setChoosenGood(goodItem){
    this.setState({choosenGood: goodItem})
  }
  addToWishes(){
    const {handleWishes}= this.props;
    const {choosenGood} = this.state;
    handleWishes(choosenGood)
  }

  addToCart(item) {
    const {handleCart} = this.props
    handleCart(item)
  }


}


GoodsList.propTypes = {
  goodsList: PropTypes.array.isRequired,
  handleCart: PropTypes.func.isRequired,
  handleWishes: PropTypes.func.isRequired
};



export default GoodsList;