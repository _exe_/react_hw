import './App.scss';
import React, { Component } from 'react';
import GoodsList from './components/Goods/GoodsList';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      goods: [],
      wishes: localStorage.getItem("wishes") || null,
      cart: localStorage.getItem("cart") || null,
    };
  }
  

  render() {
    const {goods}= this.state;
 
    return (
      <div>
        <GoodsList  handleCart={(item)=> this.handleCart(item)} handleWishes={(item)=> this.handleWishes(item)} goodsList={goods}/>
      </div>
    );
  }

  componentDidMount(){
    fetch("/goods.json")
    .then(res=> res.json())
    .then(data=> { 
        if (!localStorage["items"]) {
          localStorage.setItem("items", JSON.stringify(data.result))
          this.setState({goods: data.result})
        } else{
          this.setState({goods: data.result})
        }}
      )
    .catch(err=> console.log(err))
  }

  handleCart(item){
      if (localStorage) {
        let cart;
        if (!localStorage["cart"]) {
          cart = [];
        } else {
          cart = JSON.parse(localStorage['cart']);
        }            
        if (!(cart instanceof Array)) {cart = [];}
        cart.push(item);
        localStorage.setItem('cart', JSON.stringify(cart));
    } 
  }

  handleWishes(item){
    console.log(item)
      if (localStorage) {
        let wishes;
        if (!localStorage["wishes"]) {
          wishes = [];
        } else {
          wishes = JSON.parse(localStorage['wishes']);
        }            
        if (!(wishes instanceof Array)) {wishes = [];}
        const result = wishes.find(id=> id === item)

        if (item === result) {
          wishes = wishes.filter(id => id !== item)
          localStorage.setItem('wishes', JSON.stringify(wishes));
        } else{
          wishes.push(item);
          localStorage.setItem('wishes', JSON.stringify(wishes));

        }
    } 
  }
}

export default App;