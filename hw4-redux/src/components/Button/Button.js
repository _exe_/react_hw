import React, { Component } from 'react';
import PropTypes from 'prop-types'


class Button extends Component {
  render() {
    const {background, text, onClick, styleSelectors} = this.props;
    return (
      <>
        <button  onClick={onClick} className={styleSelectors}  style={{background: background}}>{text}</button>
      </>
    );
  }
}


Button.defaultProps = {
  styleSelectors: "btn",
}
Button.propTypes = {
  text: PropTypes.string,
  styleSelectors: PropTypes.string,
  background: PropTypes.string,
  onClick: PropTypes.func.isRequired
}

export default Button;