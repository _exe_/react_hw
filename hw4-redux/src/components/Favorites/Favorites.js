import React, { useEffect, useState} from 'react';
import FavItem from './FavItem';
import {useDispatch, useSelector} from "react-redux";
import {goodsActions, goodsSelectors} from "../../redux/goods";
import {favActions, favSelectors} from "../../redux/favorites";


const Favorites = () => {
  const dispatch = useDispatch();
  const allItems = useSelector(goodsSelectors.allGoods);
  const allFavItemsSelector = useSelector(favSelectors.allFavorites);
  const [choosenGood, setChoosenGood] = useState(null);


  const [favItems, setfavItems] = useState([]);

  useEffect( ()=> {
    dispatch(goodsActions.saveAllGoods())
  },[] )

  useEffect(()=> {
    const favItemsObj = allItems.filter((item) => allFavItemsSelector.includes(item.id));
    setfavItems(favItemsObj)
  }, [allItems, choosenGood])

  const handleFavItem = (goodItemId) =>  {
      setChoosenGood(goodItemId)
      dispatch(favActions.removeFavItem(goodItemId))
      setChoosenGood([])
  }
  

  const favItemsEl = favItems.map(item=> (
      <FavItem
        key={item.id}
        id={item.id}
        name={item.name}
        image={item.image}
        price={item.price}
        setNumber={item.setNumber}
        color={item.color}
        removeToFavProp = {(goodItemId)=> handleFavItem(goodItemId)}

      />
      )
    );

  return (
    <div>
      {favItemsEl.length !== 0 ?  favItemsEl : <h1>No items in a favorites</h1> }
    </div>
  )
}

export default Favorites
