import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';

import './Favorites.scss';

const CartItem = ({id, name, image, price, setNumber, color, removeToFavProp}) => {
  const getWishesItem = ()=> {
      removeToFavProp(id);
  };
  
  return (
    <div className="fav__item" >
        <img src={image} alt="image" className="fav__picture"/>
        <div className="fav__details">
          <p className="fav__details">Price {price}</p>
          <p className="fav__details">Set number {setNumber}</p>
          <p className="fav__details">Name {name}</p>
          <p className="fav__details">Color {color}</p>
        </div> 
        <div className="fav__action">
        <Button 
          styleSelectors={"star__active btn"}   
          onClick={getWishesItem}
        />
        </div>
    </div>
  )
}

CartItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  setNumber: PropTypes.string.isRequired,
  color: PropTypes.string,
  setChoosenGood: PropTypes.func.isRequired,
}

export default CartItem
