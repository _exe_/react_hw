import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import "./CartItem.scss";
import {modalActions} from "../../redux/modal";
import {useDispatch} from "react-redux";


const CartItem = ({id, name, image, price, setNumber, color, removeFromCart}) => {
    const dispatch = useDispatch();
      const getItem = () =>{
          handleModal()
          removeFromCart(id);
      };

    const handleModal = () => {
        dispatch(modalActions.toogleModal())
    }
  
  return (
    <div className="cart__item" >
        <img src={image} alt={image} className="cart__picture"/>
        <div className="cart__details">
          <p className="cart__details">Price {price}</p>
          <p className="cart__details">Set number {setNumber}</p>
          <p className="cart__details">Name {name}</p>
          <p className="cart__details">Color {color}</p>
        </div> 
        <div className="cart__action">
        <Button 
          styleSelectors={"btn btn-remove"} 
          text={"Remove"} 
          onClick={getItem}
        />
        </div>
    </div>
  )
}

CartItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  setNumber: PropTypes.string.isRequired,
  color: PropTypes.string,
    removeFromCart: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired
}

export default CartItem;

