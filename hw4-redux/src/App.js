import './App.scss';
import React, {useState, useEffect} from 'react';
import GoodsList from './components/Goods/GoodsList';
import Navbar from './components/Navbar/Navbar';
import AppRoutes from './routes/AppRoutes';



const App = () => {
    return (
      <>
        <Navbar/>
        <AppRoutes />
      </>
    );
}

export default App;

