import types from "./types";


const initialState = false;

const toggleModalReducer = (currentState = initialState, action) => {
    switch (action.type) {
        case types.TOGGLE_MODAL:
            return action.payload

        default:
            return currentState
    }
};


export  default toggleModalReducer;