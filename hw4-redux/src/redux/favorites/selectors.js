const selectAllFavItems = currentStore => currentStore.favorites;

export  default ({
    allFavorites: selectAllFavItems
})