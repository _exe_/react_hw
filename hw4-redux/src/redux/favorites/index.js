import reducers from "./reducers";

export {default as favSelectors} from "./selectors"
export {default as favActions} from "./actions";
export {default as favTypes} from "./types";
export  default reducers;