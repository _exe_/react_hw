const selectAllGoods = currentStore => currentStore.goods

export  default {
    allGoods: selectAllGoods
}