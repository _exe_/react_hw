const selectAllCartItems = currentStore => currentStore.cart;

export  default ({
    allCart: selectAllCartItems
})