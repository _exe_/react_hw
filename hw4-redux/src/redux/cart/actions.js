import types from "./types";

const saveCartItemAction = (cartItem) => ({
    type: types.SAVE_CART_ITEM,
    payload: cartItem
})

const removeCartItemAction = (cartItem)=> ({
    type: types.REMOVE_CART_ITEM,
    payload: cartItem
});

const saveCartItem = (cartItem) => dispatch =>  {
    dispatch(saveCartItemAction(cartItem));
}

const removeCartItem = (cartItem) => dispatch => {
    dispatch(removeCartItemAction(cartItem))
}

export  default ({
    saveCartItemAction,
    removeCartItemAction,
    saveCartItem,
    removeCartItem,
});


