const SAVE_CART_ITEM = "SAVE_CART_ITEM";
const REMOVE_CART_ITEM = "REMOVE_CART_ITEM"

export default {SAVE_CART_ITEM,  REMOVE_CART_ITEM};
