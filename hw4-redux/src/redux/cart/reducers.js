import types from "./types";

const initialState = [];

const cartReducer = (currentState = initialState, action) => {
    switch (action.type) {
        case types.SAVE_CART_ITEM:
            return [...currentState, action.payload]
        case types.REMOVE_CART_ITEM:
            return currentState.filter(item => item !== action.payload);
        default:
            return currentState
    }
};

export default cartReducer;