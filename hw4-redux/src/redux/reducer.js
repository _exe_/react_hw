import {combineReducers} from "redux";
import goodsReducer from "./goods/index";
import modalReducer from "./modal/index";
import  favoritesReducer from "./favorites/index";
import  cartReducer from "./cart/index";
export default  combineReducers({
    goods: goodsReducer,
    favorites: favoritesReducer,
    cart: cartReducer,
    modal: modalReducer
})