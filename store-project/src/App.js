import './App.scss';
import React, { Component } from 'react';
import Modal from './components/Modal/Modal';
import Button from './components/Button/Button';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      isOpen: false,
      currentModal: null,
      goods: null,
      cart: null,
      wishes: null
    };
  }

  render() {
    const {isOpen}= this.state;
    const currentModal = this.getCurrentModal();
    return (
      <div >
        {/* <Button modal={"first"} backgroundColor={"#2C52D1"} text={"Open first modal"} handleClick={(e)=> this.handleClick(e)} /> */}
        {/* <Button modal={"second"} backgroundColor={"#414450"} text={"Open second modal"} handleClick={(e)=> this.handleClick(e)} /> */}

        {isOpen && currentModal}
      </div>
    );
  }

  handleClick(newModal){
    this.setState(prevState => {
      return {
        currentModal: newModal,
        isOpen: !prevState.isOpen
      }
    })
  }


  getCurrentModal(){
    const {currentModal} = this.state;
    switch (currentModal) {
      case "first":
        return <Modal shadowClick={()=> this.shadowClick()} header={"header for modal 1"} handleClose={()=> this.handleClick()} closeButton={false} text={"text for modal 1"} actions={["ok"]}/>;
      case "second":
        return <Modal shadowClick={(e)=> this.shadowClick(e)} header={"header for modal 2"} handleClose={()=> this.handleClick()} closeButton={true} text={"text for modal 2"} actions={["ok", "cancel"]}/>;
        default:
          return null;
    }
  }

  shadowClick(){
      const {isOpen} = this.state;
      if (isOpen) {
        this.setState({ isOpen: !isOpen})
      }
    
  }
}

export default App;