import React, { Component } from 'react';

class Button extends Component {
  render() {
    const {backgroundColor, text, handleClick, modal} = this.props;
    return (
      <>
        <button className="btn" style={{backgroundColor: backgroundColor}} onClick={()=>handleClick(modal)}>{text}</button>
      </>
    );
  }
}

export default Button;