import React, { Component } from 'react';
import Button from '../Button/Button';

class Modal extends Component {
 
  render() {
    const {header, handleClose, closeButton, text, actions} = this.props;
    const actionBtn = this.getActionBtn();
    return (
      <div>
        <div onClick={(e) => this.shadowHandleClick(e)} className="shadow"></div>
        <div className="modal">
            <div className="modal__header">
              <h3>{header}</h3>
              {closeButton && <Button backgroundColor={"#8E5AFF"} text={"CLOSE MODAL"} handleClick={()=> handleClose()} />}
            </div>
          
            <p className="modal__content">{text}</p>
            <div className="modal__actions">
              {actionBtn}
          </div>
        </div>
       
      </div>
    );
  }

  shadowHandleClick(event){
    if (event.target.className === "shadow") {
      this.props.shadowClick()
    }
  }

  getActionBtn(){
    const colors = ["#5A7FFF", "#A3ADCD"];
    const {actions, handleClose} = this.props;
    const btns = actions.map((action, index) => {
      return (<Button key={index} backgroundColor={colors[index]} text={action} handleClick={()=> handleClose()}/>)
    })
    return btns;
  }

}

export default Modal;